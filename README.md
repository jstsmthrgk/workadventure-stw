# Wann?
2021-03-17 20:30 MEZ

Auf alle, die sich im [nuudel](https://nuudel.digitalcourage.de/GLkwbxmyczZpbC3s) eingetragen haben, wird bis spätestens 21:00 MEZ gewartet.

# Wo?
~~/dev/lol vspace doch lieber [jitsi](https://meet.jit.si/aaaaaaaaaaaadventure), damit wir mehrere bildschirmübertragungen gleichzeitig haben können~~

doch [bbb](https://make.mqtt.party/b/lol-nnd-am6)

# Was ist mitzubringen?
* [Tiled Map Editor](https://www.mapeditor.org/)
  * Debian: `tiled` (outdated)
  * Arch Linux: `tiled`
* [LibreSprite](https://libresprite.github.io/) oder Aseprite Pixel Art Editor
  * AppImage: [LibreSprite](https://libresprite.github.io/)
  * AUR: `libresprite` oder `aseprite`
* Ein Plan des eigenen Hackerspaces oder von etwas anderem, das man nachbauen will, wenn mehrere Leute aus dem gleichen Hackerspace dabei sind, kann z.B. eineR außen und eineR innen machen. (Oder einen Bahnhof...)
* Eine Möglichkeit die Map zu hosten, am besten mit einem einfachen Weg, um etwas Post-Processing drüberlaufen zu lassen. Ich kann auf jeden Fall helfen bei:
  * Gitlab CI + Pages
  * Github Actions + Pages
  
  
  
# Tiled & Workadventure  

https://workadventu.re/map-building/wa-maps

## New Map

requires tiled > 1.4 (zum speichern als .json)

- Orthogonal
- CSV
- Right Down
- Tile Size: 32x32
- Map-Size: NICHT infinites

unbedingt Speichern als `.json`.

Der verlinkte Start/Hauptraum sollte `main.json` heißen.

Danach in Map->MapProperties zwei "Custom Properties" hinzufügen:

- Typ String `jitsiUrl` mit Inhalt `{<apiUrl>}`
- Typ String `apiUrl` mit Inhalt `{<jitsiUrl>}`


## tileset hinzufügen

- unbedingt "embed in map" nutzen

## Layers

ein Layer muß für W.A. vorhanden sein, der Object-Layer `floorLayer`, auf dem die Figur läuft.

alles andere sind deine Tile-Layer, die oberhalb und unterhalb des floorLayer sein können.

### Start
Der Tile-Layer `start` definiert die Start Positionen.
Eines der dort gesetzten Tiles werden zufällig als Start-Position ausgewählt.
Wenn nur ein Tile gesetzt wird, dann startet das Spiel immer an dieser Stelle.

### Ausgang
Eine `exitUrl` mithilfe eines Layer-Property setzen.
1. Ein Tile-Layer erstellen
2. Layer Property erstellen: `exitUrl=meineMap.json`
3. Läuft die Spielerin über das auf dem Layer gesetzte Tile, wird die neue Map geladen

#### Sprungpunkte innerhalb einer Map
Können mithilfe von `meineMap.json#portal1` erstellt werden. (TODO wie geht das nochmal?)

## Begrenzung der Welt

1. Tileset editieren
2. Custom Property mit Typ boolean "collides" hinzufügen
3. Tiles im Set als collides=true markieren, falls der Spieler diese nicht durchlaufen dürfen soll

## Deploy on Github


1. YourPC: Folgende Datei in euer repo verzeichnis legen: https://raw.githubusercontent.com/Metalab/maptest/master/.github/workflows/build-and-deploy.yml unter ".github/workflows/build-and-deploy.yml"
2. YourPC: push and wait
3. Github-Webseite: unter Settings / Options den automatisch durch das CI script erstellten GH-Pages branch auswählen

## Deploy on Gitlab

`.gitlab-ci.yml` erstellen, lasst euch von https://git.devlol.org/devlol-systems/vspace-workadv/-/blob/master/.gitlab-ci.yml und https://gitlab.com/steyr-werke/workadventure-stw/-/blob/master/.gitlab-ci.yml inspirieren


# Adventures der Spaces

- [Overview](https://aaaaaaaaaaaadventure.gitlab.io/)

- r3 repo: https://github.com/realraum/workadventur3space
  - https://realraum.github.io/workadventur3space/main.json

~~- /usr/space: https://github.com/pludi/WorkAdventure/actions~~
~~  - https://pludi.github.io/WorkAdventure/usr_space_innen.json~~ (offline, wird neu gemacht)
  
  https://git.devlol.org/devlol-systems/vspace-workadv/-/tree/master/
  
  https://gitlab.com/steyr-werke/workadventure-stw/
 